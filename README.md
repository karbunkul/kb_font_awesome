# Описание
Библиотека Font Awesome Sass для Drupal 7 
# Установка
`sudo npm install && sudo bower install && gulp`

# Использование
`drupal_add_library(‘kb_font_awesome’, ‘font-awesome’);`

# Автор 
© 2015 Александр Походюн ([Karbunkul](mailto://karbunkul@yourtask.ru))
# Лицензия
MIT