/**
 * Created by karbunkul on 03/03/15.
 */

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', ['bower', 'sass']);

/**
 * Get last version from bower
 */
gulp.task('bower', function () {
    gulp.src('bower_components/font-awesome-sass/assets/**')
        .pipe(gulp.dest('assets'))

    gulp.src('assets/stylesheets/font-awesome/_variables.scss')
        .pipe(gulp.dest('assets/scss'));
});

/**
 * SCSS compile
 */
gulp.task('sass', function () {
    gulp.src('assets/scss/font-awesome.scss')
        .pipe(sass())
        .pipe(gulp.dest('assets/css'));
});